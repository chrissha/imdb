import requests
import json
import os
import sys
import argparse


from apikey import _apikey
_url = 'http://www.omdbapi.com/'
_poster_url = 'http://img.omdbapi.com/'


def _check_response(response):
    """
    Checks the HTTP response for the request. If a invalid response is returned
    the program will print the error and exit.

    params:
        response - The response object.
    """
    if response.status_code == requests.codes['\o/']:
        return 
    else:
        response.raise_for_status()
        exit(-1)


def _save_json(movie_json):
    """
    Saves the json object to a file. The name of the file will be the movie
    title without spaces and all lowercase ending in '.json'.

    params:
        json - The json object to save.
    """
    if not os.path.exists('movies'):
        os.makedirs('movies')
    trimmed_title = movie_json['Title'].replace(" ", "").lower()
    with open('movies/' + trimmed_title + '.json', 'w+') as outfile:
        json.dump(movie_json, outfile)


def _load_json(filename):
    """
    Loads a json file from 'imdb/movies'. Assumes that the file exists.

    params:
        filename - the name of the json file.
    returns:
        A json object with information about the movie.
    """
    with open('movies/' + filename + '.json') as json_file:
        movie_json = json.load(json_file)
        return movie_json


def _request_imdb(movie_title):
    """
    Uses omdbapi.com to find information about a movie and returns information
    about the movie as a json file. If an error occurs the program will print
    the error and exit.

    params:
        movie_title - The title of a movie.
    returns:
        A json object with information about the movie.
    """
    params = {'apikey': _apikey, 't': movie_title}
    response = requests.get(_url, params=params)
    _check_response(response)
    movie_json = response.json()

    if movie_json['Response'] == "False":
        print("Error:", movie_json['Error'])
        sys.exit(-1)

    return movie_json


def _print_movie_info(movie_json):
    """
    Prints out information about a movie.

    params:
        movie_json - The json file of a movie.
    """
    print(  "Title:",       movie_json['Title'],
            "\nReleased:",  movie_json['Released'],
            "\nGenre:",      movie_json['Genre'],
            "\nRuntime:",   movie_json['Runtime'])  


def lookup_movie(movie_title: str):
    """
    Searches for a movie and returns information about it as a json object. 
    Movie data is retrieved from omdbapi.com and saved locally for further use.

    params:
        movie_title - Title of the movie as a string.
    returns:
        A json object containing information about the movie
    """
    trimmed_title = movie_title.replace(" ", "").lower()

    if os.path.exists('movies/' + trimmed_title + '.json'):
        movie_json = _load_json(trimmed_title)
    else:
        movie_json = _request_imdb(movie_title)
        _save_json(movie_json)

    return movie_json


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Look up a movie on imdb.")
    parser.add_argument('title', help="The title of the movie")
    args = parser.parse_args()
    movie_json = lookup_movie(vars(args)['title'])
    _print_movie_info(movie_json)
